from django import forms
from profiles.models import UserProfile, Note, Color, Category
from django.contrib.auth.models import User
import datetime
from django.contrib.auth import authenticate


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        if not self.errors:
            user = authenticate(username=cleaned_data['username'], password=cleaned_data['password'])
            if user is None:
                raise forms.ValidationError('Username and password do not match')
            self.user = user
        return cleaned_data

    def get_user(self):
        return self.user


class RegistrationForm(forms.Form):
    username = forms.CharField()
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    email = forms.EmailField(max_length=30)
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
            User.objects.get(username=username)
            raise forms.ValidationError(u'Such a user already exists')
        except Exception:
            return self.cleaned_data

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 == password2:
            return self.cleaned_data
        else:
            raise forms.ValidationError(u'Passwords do not match')


class UserProfileForm(forms.Form):
    birthday = forms.DateField(widget=forms.widgets.DateInput(format='%Y-%m-%d'))
    avatar = forms.ImageField()


class EditUser(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class EditUserForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['birthday', 'avatar']


class CreateCategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ['category_name']


class CreateColorForm(forms.ModelForm):
	class Meta:
		model = Color
		fields = ['color_name']


class CreateNoteForm(forms.Form):
	title = forms.CharField()
	category = forms.ModelMultipleChoiceField(queryset=Category.objects.all())
	#category = forms.ChoiceField(
	#	choices=[(category,category) for category in Category.objects.all()])
	content = forms.CharField(widget=forms.Textarea)
	color = forms.ChoiceField(
		choices=[(color,color) for color in Color.objects.all()])
	media = forms.FileField()
	pub_date = forms.DateField(initial=datetime.datetime.now)
	publish = forms.BooleanField()



class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['title', 'category', 'content', 'color',
        		 'media', 'pub_date']