from django.contrib import admin
from profiles.models import UserProfile, Note, Color, Category

class UserAdmin(admin.ModelAdmin):
	list_display = ('user', 'birthday', 'avatar')

class NoteAdmin(admin.ModelAdmin):
	list_display = ('title', 'content', 'user',
					'media', 'pub_date', 'publish')


admin.site.register(UserProfile, UserAdmin)
admin.site.register(Note, NoteAdmin)
admin.site.register(Color)
admin.site.register(Category)
