from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class UserProfile(models.Model):
    user = models.OneToOneField(User, unique=True, related_name='profile')
    birthday = models.DateField(blank=True, null=True)
    avatar = models.ImageField(upload_to='image/avatar', blank=True)

    def __str__(self):
        return self.user.username

    def get_user(self):
        return self.user

    def get_profile(self):
        return {'birthday': self.birthday, 'avatar': self.avatar}

'''
def create_profile(sender, **kwargs):
    if kwargs['created']:
        UserProfile.objects.create(user=kwargs['instance'])
models.signals.post_save.connect(create_profile, sender=User)
'''

class Color(models.Model):
	color_name = models.CharField(max_length=255, blank=True)

	def __unicode__(self):
		return self.color_name


class Category(models.Model):
	category_name = models.CharField(max_length=255, blank=True)

	def __unicode__(self):
		return self.category_name


class Note(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=255, null=True)
    pub_date = models.DateField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    color = models.ForeignKey(Color, blank=True)
    category = models.ManyToManyField(Category, blank=True)
    media = models.FileField(upload_to='users/files', blank=True)
    publish = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return self.title


    