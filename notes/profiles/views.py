from django.shortcuts import render, render_to_response
from django.template import RequestContext
from profiles.forms import UserProfileForm, LoginForm, RegistrationForm
from profiles.forms import EditUser, CreateNoteForm, NoteForm, EditUserForm
from profiles.forms import CreateCategoryForm, CreateColorForm
from profiles.models import UserProfile, Category, Note, Color
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic import TemplateView, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, CreateView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, UpdateView
from profiles.actions import get_users
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


class AllUsersViews(ListView):
	''' All users list '''
	model = User
	template_name = 'profiles/user_list.html'

	def post(self, request, *args):
		if not request.user.is_authenticated():
			return HttpResponseRedirect('/log_in/')
		else:
		 	context = request.POST.get('text','')
			user = User.objects.filter(username__icontains=context)
			return render(request, 'profiles/user_list.html', 
		 				{'user_list': user }) 


class CreateNoteView(CreateView):
	'''Creating new note'''
	model = Note
	fields = ['title', 'category', 'content','pub_date', 
			'color', 'media', 'publish']
	success_url = '/add_note/'
	template_name = 'profiles/new_note.html'


	def form_valid(self, form):
		''' method for valid form note in POST '''
		form.instance.user = self.request.user
		super(CreateNoteView, self).form_valid(form)
		return HttpResponseRedirect('/profile/')


class NoteView(DetailView):
	''' Detail about note '''
	model = Note
	template_name = 'profiles/note.html'



class NoteUpdate(UpdateView):
	''' Updating note '''
	model = Note
	fields = ['title', 'category', 'content','pub_date', 
			'color', 'media', 'publish']
	template_name = 'profiles/note_update.html'
	success_url = '/profile/'


class NoteDelete(DeleteView):
	''' Delete note '''
	model = Note
	success_url = '/profile/'


class CategoryView(CreateView):
	''' FormView for creating new category '''
	template_name = 'profiles/new_category.html'
	success_url = '/new_category/'
	form_class = CreateCategoryForm

	def form_valid(self, form):
		''' method for valid form category in POST '''
		super(CategoryView, self).form_valid(form)
		return HttpResponseRedirect('/add_note/')


class ColorView(CreateView):
	''' FormView for creating new color '''
	template_name = 'profiles/new_color.html'
	success_url = '/new_color/'
	form_class = CreateColorForm

	def form_valid(self, form):
		''' method for valid form color in POST '''
		super(ColorView, self).form_valid(form)
		return HttpResponseRedirect('/add_note/')


def registr(request):
	''' Function for registration users '''
	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		formProfile = UserProfileForm(request.POST, request.FILES)
		if form.is_valid():
			user = User.objects.create_user(username=request.POST['username'], email=request.POST['email'])
			user.first_name = request.POST['first_name']
			user.last_name = request.POST['last_name']
			user.set_password(request.POST['password1'])
			user.save()

			profile = user.profile
			profile.birthday = request.POST['birthday']
			profile.avatar = request.FILES['avatar']
			profile.save()

			user = authenticate(username=request.POST['username'], password=request.POST['password1'])
			login(request, user)
			return HttpResponseRedirect('/')
	else:
		form = RegistrationForm()
		formProfile = UserProfileForm()
		return render(request, 'profiles/registr.html', 
					{'form': form, 'formP': formProfile})


def log_in(request):
	''' Function for login users '''
	if not request.user.is_authenticated():
		if request.method == 'POST':
			form = LoginForm(request.POST)
			if form.is_valid():
				if form.get_user():
					authenticate(username=form.get_user().username, password=form.get_user().password)
					login(request, form.get_user())
					return HttpResponseRedirect('/profile/')
		else:
			form = LoginForm()
		return render(request, 'profiles/login.html', {'form': form})
	else:
		return HttpResponseRedirect('/profile/')


@login_required
def log_out(request):
	''' Function for logout users '''
	logout(request)
	return HttpResponseRedirect('/')


def profile(request):
	''' Function for get self profile users '''
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	else:
		context = {'entries_list': Note.objects.filter(user=request.user).order_by('-pub_date','-id')}
		return render_to_response('profiles/profile.html', RequestContext(request, context))

	
@login_required
def user_profile(request, pk):
    ''' Function for get profiles any users '''
    try:
        user = User.objects.select_related("profile").get(pk=pk)
    except User.DoesNotExist:
        raise Http404
    user_profile = {'id': pk}
    if (id) == request.user.id:
        return note(request)
    else:
        return render(request, 'profiles/profile.html', {'user': user, 'entries_list': Note.objects.filter(user=user).order_by('-pub_date','-id'),
                                    'user_profile': user_profile})

@login_required
def edit_profile(request):
    ''' Function for edit profile user '''
    user = request.user
    if request.method == 'POST':
        form = EditUser(request.POST, instance=user)
        formProfile = EditUserForm(request.POST, request.FILES, instance=user.profile)
        if form.is_valid() and formProfile.is_valid():
            form.save()
            formProfile.save()
            return HttpResponseRedirect('/profile/')
    else:
        form = EditUser(instance=user)
        formProfile = EditUserForm(instance=user.profile)
    return render(request, 'profiles/edit_profile.html', 
    			{'form': form, 'formP': formProfile})
