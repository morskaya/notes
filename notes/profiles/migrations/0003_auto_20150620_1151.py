# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20150620_1144'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='color',
        ),
        migrations.AddField(
            model_name='note',
            name='color',
            field=models.ForeignKey(to='profiles.Color', blank=True),
        ),
    ]
