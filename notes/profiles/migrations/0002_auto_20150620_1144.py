# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='font',
        ),
        migrations.RemoveField(
            model_name='note',
            name='tag',
        ),
        migrations.AlterField(
            model_name='note',
            name='category',
            field=models.ManyToManyField(to='profiles.Category', blank=True),
        ),
        migrations.AlterField(
            model_name='note',
            name='color',
            field=models.OneToOneField(blank=True, to='profiles.Color'),
        ),
        migrations.AlterField(
            model_name='note',
            name='media',
            field=models.FileField(upload_to=b'users/files', blank=True),
        ),
        migrations.DeleteModel(
            name='Font',
        ),
        migrations.DeleteModel(
            name='Media',
        ),
        migrations.DeleteModel(
            name='Tag',
        ),
    ]
