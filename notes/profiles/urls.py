from django.conf.urls import url
from django.conf import settings
from profiles.views import AllUsersViews, user_profile, NoteUpdate, NoteDelete
from profiles.views import log_in, registr, log_out, profile, CreateNoteView, NoteView
from profiles.views import CategoryView, ColorView, edit_profile

urlpatterns = [
	url(r'^$', log_in, name='log_in'),
	url(r'^registr/$', registr, name='registr'),
	url(r'^profile/$', profile, name='profile'),
	url(r'^edit_profile/$', edit_profile, name='edit_profile'),
	url(r'^note/(?P<pk>\d+)/$', NoteView.as_view(), name='note'),
	url(r'^user_list/$', AllUsersViews.as_view(), name='user_list'),
	url(r'^user_profile/(?P<pk>\d+)/$', user_profile, name='user_profile'),
	url(r'^add_note/$', CreateNoteView.as_view(), name='add_note'),
	url(r'^new_category/$', CategoryView.as_view(), name='new_category'),
	url(r'^new_color/$', ColorView.as_view(), name='new_color'),
	url(r'^note_update/(?P<pk>\d+)/$', NoteUpdate.as_view(), name="note_update"),
	url(r'^note_delete/(?P<pk>\d+)/$', NoteDelete.as_view(), name="note_delete"),
	url(r'^log_out/$', log_out, name='log_out'),	
]