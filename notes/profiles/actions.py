from profiles.models import UserProfile

def get_users():
	return [user for user in UserProfile.objects.all()]
